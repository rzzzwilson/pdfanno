# pdfanno

A simple system to easily generate annotated PDF files.

    Usage:  pdfanno [-h] <doc_name> <out_file>
    
    where -h          print this help and stop
          -l          list the know doc_names and stops
          <doc_name>  is the doc name of the PDF file to annotate
          <out_file>  is the name of the output annotated file

## Data Directory

This program uses configuration and data files stored in the *~/.config/pdfanno*
directory.  The files stored there are:

* JSON configuration files
* PDF template files

## JSON Configuration Files

There are three JSON files stored in *~/.config/pdfanno*:

* pdfanno.json
* labels.json
* annotations.json

The file *pdfanno.json* contains a dictionary that links the file **name**
to the actual **filename**.  So if *pdfanno.json* contains:

    {
      "TM30_CM": "TM30_CM.pdf",
      "TM47_CM": "TM47_CM.pdf",
      "TM7_CM": "TM7_CM.pdf",
      "TM8_CM": "TM8_CM.pdf"
    }

then the user may use names like *TM47_CM* on the command line to annotate
the file *TM47_CM.pdf* in the *~/.config/pdfanno* directory.

The file *labels.json* contains basic data about labels that can be used
to annotate a file.  The file contains a dictionary with entries that have
a key of the label name, with the corresponding value being the actual textual
data for the label.

    {
      "address": "175 M.1 Kanklong Cholprathan Rd., T. Changpuek, A. Muang, Chiang Mai, 50300",
      "day": "$DAY",
      "first_name": "Ross",
      "last_name": "Wilson",
      "month": "$MONTH",
      "year": "$YEAR"
    }

The textual data may have the form `$DAY`.  The leading `$` indicates that this is a
dynamic value and the value is replaced by whatever the `$aaa` indicates:

| label       | replaced with        |
| :---------  | :------------------- |
| $DAY        | the day number of the month |
| $MONTH      | the month name ("June") in the year |
| $YEAR       | the year number (YYYY) |

The file *annotations.json* holds the annotations for a particular file.
The file is a dictionary with the key being the file name and the corresponding
value being a list of annotatiopn entries, where each entry is a list:

    [label, page, [x1,y1,x2,y2]]

So the *first_name* label in this entry for the "TM7_CM" file:

      "TM7_CM": [["first_name", 0, [10, 710, 110, 725]],
                 ["address", 0, [10, 730, 650, 745]],
                 ["day", 0, [10, 690, 100, 705]],
                 ["month", 0, [10, 670, 210, 685]],
                 ["year", 0, [10, 650, 210, 665]]
                ]


is

    ["first_name", 0, [10, 710, 110, 725]]

which tells *pdfanno* to draw the *first_name* annotation on page 0
inside the rectangle defined by (x1,y1) and (x2,y2).

## PDF Template Files

These are just sample PDF files that are to be annotated.  They are stored in
the *~/.config/pdfanno* directory because their names must match config data
in the JSON files.

# mk_pdfanno.py

This program was used to create the initial JSON files in *~/.config/pdfanno*.


